-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 02, 2012 at 11:47 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `grocery_mate`
--

-- --------------------------------------------------------

--
-- Table structure for table `aisle`
--

DROP TABLE IF EXISTS `aisle`;
CREATE TABLE IF NOT EXISTS `aisle` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `aisle_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `aisle`
--

INSERT INTO `aisle` (`id`, `aisle_name`) VALUES
(1, 'pantry'),
(2, 'liquor'),
(3, 'pet'),
(4, 'baby'),
(5, 'health & beauty'),
(6, 'household'),
(7, 'freezer'),
(8, 'fridge'),
(9, 'meat'),
(10, 'fresh'),
(11, 'tobacco');

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

DROP TABLE IF EXISTS `backups`;
CREATE TABLE IF NOT EXISTS `backups` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `backups`
--

INSERT INTO `backups` (`id`, `date_time`, `file`) VALUES
(1, '2012-07-18 09:24:20', 'admin/resources/backups/backup-1342567460.zip');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`) VALUES
(6, 'Arnott''s'),
(1, 'arnotts'),
(4, 'Coca Cola'),
(7, 'Coles'),
(2, 'Heinz'),
(5, 'Kellogg''s'),
(3, 'Spaghetti'),
(8, 'Stayfree');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(150) DEFAULT NULL,
  `aisle_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aisle_id` (`aisle_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=121 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `aisle_id`) VALUES
(1, 'baking', 1),
(2, 'biscuits', 1),
(3, 'breakfast', 1),
(4, 'canned food', 1),
(5, 'chips, snacks & nuts', 1),
(6, 'bakery', 10),
(7, 'delicatessen', 10),
(8, 'entertaining packs', 10),
(9, 'fresh flowers', 10),
(10, 'fruit', 10),
(11, 'beef', 9),
(12, 'deli meats', 9),
(13, 'game', 9),
(14, 'lamb', 9),
(15, 'mince', 9),
(16, 'cheese', 8),
(17, 'cream', 8),
(18, 'custard', 8),
(19, 'desserts', 8),
(20, 'dips & antipasto', 8),
(21, 'chips &  wedges', 7),
(22, 'desserts', 7),
(23, 'frozen fruit', 7),
(24, 'frozen vegitables', 7),
(25, 'heat & eat', 7),
(26, 'Condiments', 1),
(27, 'Confectionery', 1),
(28, 'Cooking Sauces', 1),
(29, 'Desserts', 1),
(30, 'Drinks', 1),
(31, 'Eggs', 1),
(32, 'Health Foods & Organics', 1),
(33, 'International Foods', 1),
(34, 'Jams & Spreads', 1),
(35, 'Oil, Herbs & Dressing', 1),
(36, 'Rice, Pasta & Instant Meals', 1),
(37, 'Soup', 1),
(38, 'Salad & Herbs', 10),
(39, 'Seafood', 10),
(40, 'Vegetables', 10),
(41, 'Pork', 9),
(42, 'Poultry', 9),
(43, 'Ready Meals', 9),
(44, 'Sausages & BBQ Packs', 9),
(45, 'Seafood', 9),
(46, 'Stir Fry & Diced', 9),
(47, 'Veal', 9),
(48, 'Deli Meats', 8),
(49, 'Eggs', 8),
(50, 'Fresh Meals', 8),
(51, 'Fruit Juice', 8),
(52, 'Milk', 8),
(53, 'Seafood', 8),
(54, 'Spreads', 8),
(55, 'Yoghurt', 8),
(56, 'Ice', 7),
(57, 'Ice Cream & Yoghurt', 7),
(58, 'Pastry', 7),
(59, 'Poultry', 7),
(60, 'Appliances & Electrical', 6),
(61, 'Bathroom & Toilet', 6),
(62, 'Cleaning', 6),
(63, 'Clothing & Footwear', 6),
(64, 'Computer & Photo', 6),
(65, 'Entertainment', 6),
(66, 'Garden & Outdoor', 6),
(67, 'Homeware', 6),
(68, 'Kitchen', 6),
(69, 'Laundry', 6),
(70, 'Party Supplies', 6),
(71, 'Stationery & Wrapping', 6),
(72, 'Toys & Games', 6),
(73, 'Workshop', 6),
(74, 'Cosmetics', 5),
(75, 'Dental Care', 5),
(76, 'Deodorants', 5),
(77, 'Feminine Hygiene', 5),
(78, 'Hair Care', 5),
(79, 'Hair Removal', 5),
(80, 'Incontinence Needs', 5),
(81, 'Medicinal', 5),
(82, 'Men''s Grooming', 5),
(83, 'Skin Care', 5),
(84, 'Soap & Body Wash', 5),
(85, 'Tissues', 5),
(86, 'Toilet Paper', 5),
(87, 'Travel Products', 5),
(88, 'Vitamins', 5),
(89, 'Baby Food', 4),
(90, 'Baby Toys', 4),
(91, 'Baby Wipes', 4),
(92, 'Bath Time', 4),
(93, 'Clothing', 4),
(94, 'Feeding', 4),
(95, 'Gift Packs', 4),
(96, 'Nappies', 4),
(97, 'Nappy Change', 4),
(98, 'Safety', 4),
(99, 'Teats & Soothers', 4),
(100, 'Bird', 3),
(101, 'Cat Accessories', 3),
(102, 'Cat Food', 3),
(103, 'Dog Accessories', 3),
(104, 'Dog Food', 3),
(105, 'Fish', 3),
(106, 'Guinea Pig & Rabbit', 3),
(107, 'Pet Litter', 3),
(108, 'Beer & Cider', 2),
(109, 'Cask Wine', 2),
(110, 'Fortified Wine', 2),
(111, 'Ready To Drink', 2),
(112, 'Red Wine', 2),
(113, 'Sparkling Wine', 2),
(114, 'Spirits', 2),
(115, 'White Wine', 2),
(116, 'Cigarettes', 11),
(117, 'Cigars', 11),
(118, 'Loose Tobacco', 11),
(119, 'Papers & Filters', 11),
(120, 'Stop Smoking', 11);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `table` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `parent_module_id` int(16) NOT NULL,
  `field_uid` int(16) NOT NULL,
  `field_slug` int(16) NOT NULL,
  `field_parent` int(16) NOT NULL,
  `field_orderby` int(16) NOT NULL,
  `orderby_direction` enum('DESC','ASC') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DESC',
  `management_width` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locked` tinyint(1) NOT NULL,
  `lock_records` tinyint(1) NOT NULL,
  `core_module` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=76 ;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `table`, `slug`, `parent_module_id`, `field_uid`, `field_slug`, `field_parent`, `field_orderby`, `orderby_direction`, `management_width`, `type`, `locked`, `lock_records`, `core_module`) VALUES
(3, 'Modules', 'modules', 'modules', 0, 6, 14, 10, 14, 'ASC', '30%', 'module', 1, 1, 1),
(4, 'Fields', 'modules_fields', 'fields', 3, 17, 20, 0, 19, 'DESC', '30%', 'module_field', 1, 1, 1),
(5, 'Validation', 'modules_fields_validation', 'validation', 3, 39, 40, 0, 39, 'DESC', '30%', 'module_field_validation', 1, 1, 1),
(62, 'Users', 'users', 'users', 0, 362, 371, 0, 0, 'DESC', '20%', 'user', 0, 0, 0),
(63, 'Groups', 'users_groups', 'groups', 62, 379, 380, 0, 0, 'ASC', '20%', 'user_group', 0, 0, 0),
(64, 'Meta', 'users_meta', 'meta', 62, 384, 385, 0, 0, 'DESC', '20%', 'user_meta', 0, 0, 0),
(65, 'Backups', 'backups', 'backups', 0, 388, 390, 0, 0, 'DESC', '20%', 'backup', 0, 0, 0),
(66, 'Categories', 'category', 'categories', 0, 391, 392, 0, 0, 'DESC', '20%', 'category', 0, 0, 0),
(67, 'Aisles', 'aisle', 'aisles', 66, 394, 395, 0, 0, 'DESC', '20%', 'aisle', 0, 0, 0),
(69, 'Products', 'products', 'products', 0, 398, 405, 0, 0, 'DESC', '20%', 'product', 0, 0, 0),
(68, 'Brands', 'brands', 'brands', 0, 396, 397, 0, 0, 'DESC', '20%', 'brand', 0, 0, 0),
(70, 'Product Types', 'product_types', 'product-types', 69, 406, 407, 0, 0, 'DESC', '20%', 'product_type', 0, 0, 0),
(71, 'Similar Products', 'products_similar', 'similar-products', 69, 408, 409, 0, 0, 'DESC', '20%', 'similar_product', 0, 0, 0),
(72, 'Product Sales', 'product_sales', 'product-sales', 69, 411, 412, 0, 0, 'DESC', '20%', 'product_sales', 0, 0, 0),
(73, 'Product Sale Exceptions', 'product_sale_exceptions', 'product-sale-exceptions', 69, 413, 423, 0, 0, 'DESC', '20%', 'product_sale_exception', 0, 0, 0),
(74, 'Stores', 'stores', 'stores', 0, 425, 426, 0, 0, 'DESC', '20%', 'store', 0, 0, 0),
(75, 'Supermarket', 'supermarkets', 'supermarkets', 74, 431, 432, 0, 0, 'DESC', '20%', 'supermarket', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules_fields`
--

DROP TABLE IF EXISTS `modules_fields`;
CREATE TABLE IF NOT EXISTS `modules_fields` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `order` tinyint(2) NOT NULL,
  `module_id` int(32) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `editable` tinyint(1) NOT NULL,
  `display_width` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `tooltip` text COLLATE utf8_unicode_ci NOT NULL,
  `fieldset` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `specific_search` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=433 ;

--
-- Dumping data for table `modules_fields`
--

INSERT INTO `modules_fields` (`id`, `order`, `module_id`, `name`, `label`, `type`, `editable`, `display_width`, `tooltip`, `fieldset`, `specific_search`) VALUES
(6, 1, 3, 'id', 'Id', 'id', 0, '10%', '', '', 0),
(8, 3, 3, 'table', 'Table', '', 1, '', '', '', 0),
(9, 6, 3, 'slug', 'Slug', '', 1, '', '', '', 0),
(10, 7, 3, 'parent_module_id', 'Module Parent', 'module', 1, '', '', '', 0),
(11, 8, 3, 'field_uid', 'Id field', 'module_field_current', 1, '', '', '', 0),
(12, 12, 3, 'orderby_direction', 'Default order direction', 'orderby_direction', 1, '', '', '', 0),
(13, 13, 3, 'management_width', 'Options field width', 'text_small', 1, '', '', '', 0),
(14, 2, 3, 'name', 'Name', '', 1, '55%', '', '', 0),
(15, 10, 3, 'field_parent', 'Parent field', 'module_field_current', 1, '', '', '', 0),
(16, 9, 3, 'field_slug', 'Slug field', 'module_field_current', 1, '', '', '', 0),
(25, 11, 3, 'field_orderby', 'Default order by field', 'module_field_current', 1, '', '', '', 0),
(35, 9, 3, 'type', 'Type definition', '', 1, '', '', '', 0),
(50, 10, 3, 'locked', 'Locked', 'yes_no', 1, '', '', '', 0),
(94, 10, 3, 'core_module', 'Core module', 'yes_no', 1, '', '', '', 0),
(91, 9, 3, 'lock_records', 'Lock Records', 'yes_no', 1, '', '', '', 0),
(17, 1, 4, 'id', 'ID', 'id', 0, '10%', '', '', 0),
(18, 3, 4, 'name', 'Name', '', 1, '', '', '', 0),
(19, 6, 4, 'module_id', 'Module', 'module', 1, '30%', '', '', 1),
(20, 2, 4, 'label', 'Label', '', 1, '30%', '', '', 0),
(21, 5, 4, 'type', 'Type', 'type', 1, '', '', '', 0),
(22, 8, 4, 'editable', 'Editable?', 'yes_no', 1, '', '', '', 0),
(23, 7, 4, 'display_width', 'Display width', 'text_small', 1, '', '', '', 0),
(34, 4, 4, 'order', 'Order', 'text_small', 1, '', '', '', 0),
(312, 2, 4, 'tooltip', 'Tooltip', 'textarea_small', 1, '', '', '', 0),
(321, 3, 4, 'fieldset', 'Fieldset', '', 1, '', 'Fields of the same fieldset will be grouped together', '', 0),
(327, 10, 4, 'specific_search', 'Specific search', 'yes_no', 1, '', 'If selected this column will be given it''s own search field in forms', '', 0),
(39, 1, 5, 'id', 'ID', 'id', 0, '10%', '', '', 0),
(40, 2, 5, 'name', 'Rule Name', 'module_validation_rule', 1, '30%', '', '', 0),
(43, 4, 5, 'field_id', 'Field', 'module_field', 1, '30%', '', '', 0),
(362, 1, 62, 'id', 'ID', 'id', 0, '', '', '', 0),
(363, 1, 62, 'email', 'Email', '', 1, '45%', '', '', 0),
(364, 0, 62, 'email_verified', 'Email Verified', 'yes_no', 1, '', 'If <a href="?module_path=dashboard/settings">Email Verification</a> is enabled then this shows whether or not the user has verified their email address.', '', 1),
(365, 4, 62, 'password', 'Password', 'password', 1, '', '', '', 0),
(366, 8, 62, 'lastlogin', 'Last login', 'datetime_static', 1, '', '', '', 0),
(367, 12, 62, 'lastip', 'Last IP used', 'static', 1, '', '', '', 0),
(368, 5, 62, 'group_id', 'User Group', 'user_group', 1, '', '', '', 0),
(369, 11, 62, 'avatar', 'Profile image', 'file_image', 1, '', '', '', 0),
(370, 7, 62, 'date_registered', 'Date registered', 'datetime_static', 1, '30%', '', '', 0),
(371, 1, 62, 'display_name', 'Display name', '', 1, '', '', '', 0),
(372, 5, 62, 'temporary_password', 'Temporary password', 'hidden', 0, '', '', '', 0),
(373, 12, 62, 'facebook_id', 'Facebook ID', 'static', 1, '', 'If the user has linked their account with Facebook or uses Facebook to login, this is their Facebook account ID.', '', 0),
(374, 13, 62, 'twitter_id', 'Twitter ID', 'static', 1, '', 'If the user has linked their account with Twitter or uses Twitter to login, this is their Twitter account ID.', '', 0),
(375, 14, 62, 'yahoo_id', 'Yahoo ID', 'static', 1, '', 'If the user has linked their account with Yahoo or uses Yahoo to login, this is their Yahoo account ID.', '', 0),
(376, 15, 62, 'windows_live_id', 'Windows Live ID', 'static', 1, '', 'If the user has linked their account with Windows Live or uses Windows Live to login, this is their Windows Live account ID.', '', 0),
(377, 16, 62, 'google_id', 'Google ID', 'static', 1, '', 'If the user has linked their account with Google or uses Google to login, this is their Google account ID.', '', 0),
(378, 0, 62, 'type', 'Type', 'user_type', 0, '1%', '', '', 0),
(379, 1, 63, 'id', 'ID', 'id', 0, '', '', '', 0),
(380, 2, 63, 'name', 'Name', '', 1, '30%', '', '', 0),
(381, 3, 63, 'admin', 'Users are Admins?', 'yes_no', 1, '', '', '', 0),
(382, 4, 63, 'default_value', 'Default group', 'yes_no', 1, '', '', '', 0),
(383, 5, 63, 'access_level', 'Access Level', 'integer', 1, '', '', '', 0),
(384, 1, 64, 'id', 'ID', 'id', 0, '', '', '', 0),
(385, 2, 64, 'key', 'Key', '', 1, '20%', '', '', 0),
(386, 3, 64, 'value', 'Value', 'textarea_small', 1, '', '', '', 0),
(387, 4, 64, 'user', 'User', 'user', 1, '30%', '', '', 0),
(388, 1, 65, 'id', 'ID', 'id', 0, '', '', '', 0),
(389, 2, 65, 'date_time', 'Date & Time', 'datetime_now', 1, '40%', '', '', 0),
(390, 3, 65, 'file', 'File', 'file', 1, '', '', '', 0),
(391, 1, 66, 'id', 'ID', 'id', 0, '', '', '', 0),
(392, 2, 66, 'category_name', 'Name', '', 1, '50%', '', '', 0),
(393, 3, 66, 'aisle_id', 'Aisle', 'aisle', 1, '', '', '', 0),
(394, 1, 67, 'id', 'Aisle', 'id', 0, '', '', '', 0),
(395, 2, 67, 'aisle_name', 'Name', '', 1, '50%', '', '', 0),
(396, 1, 68, 'id', 'Brand', 'id', 0, '', '', '', 0),
(397, 2, 68, 'name', 'Name', '', 1, '50%', '', '', 0),
(398, 1, 69, 'id', 'Product', 'id', 0, '', '', '', 0),
(399, 2, 69, 'type', 'Type', 'type', 1, '20%', '', '', 0),
(400, 3, 69, 'ean', 'EAN', '', 1, '20%', '', '', 0),
(401, 4, 69, 'brand', 'Brand', 'brand', 1, '20%', '', '', 0),
(402, 5, 69, 'measure', 'Measure', '', 1, '20%', '', '', 0),
(403, 6, 69, 'uom', 'UOM', '', 1, '20%', '', '', 0),
(404, 7, 69, 'image', 'Image', 'file_image', 1, '', '', '', 0),
(405, 8, 69, 'category_id', 'Category', 'category', 1, '20%', '', '', 0),
(406, 1, 70, 'id', 'Type', 'id', 0, '', '', '', 0),
(407, 2, 70, 'name', 'Name', '', 1, '50%', '', '', 0),
(408, 1, 71, 'id', 'ID', 'id', 0, '', '', '', 0),
(409, 2, 71, 'product_id', 'Product #1', 'product', 1, '50%', '', '', 0),
(410, 3, 71, 'similar_to_product_id', 'Product #2', 'product', 1, '50%', '', '', 0),
(411, 1, 72, 'id', 'ID', 'id', 0, '', '', '', 0),
(412, 2, 72, 'product_id', 'Product', 'product', 1, '20%', '', '', 0),
(413, 3, 72, 'supermarket_id', 'Supermarket', 'supermarket', 1, '20%', '', '', 0),
(414, 4, 72, 'start_date', 'Start Date', 'date', 1, '20%', '', '', 0),
(415, 5, 72, 'end_date', 'End Date', 'date', 1, '20%', '', '', 0),
(416, 6, 72, 'price', 'Price', '', 1, '', '', '', 0),
(417, 7, 72, 'special_price', 'Special Price', '', 1, '20%', '', '', 0),
(418, 8, 72, 'bogo_buy_count', 'BOGO Buy Count', 'integer', 1, '', '', '', 0),
(419, 9, 72, 'bogo_buy_price', 'BOGO Buy Price', '', 1, '', '', '', 0),
(420, 10, 72, 'bogo_get_count', 'BOGO Get Count', 'integer', 1, '', '', '', 0),
(421, 11, 72, 'bogo_get_price', 'BOGO Get Price', '', 1, '', '', '', 0),
(422, 1, 73, 'id', 'ID', 'id', 0, '', '', '', 0),
(423, 2, 73, 'product_sale_id', 'Product', 'product_sale', 1, '50%', '', '', 0),
(424, 3, 73, 'exception_store_id', 'Store', 'store', 1, '50%', '', '', 0),
(425, 1, 74, 'id', 'ID', 'id', 0, '', '', '', 0),
(426, 2, 74, 'supermarket_id', 'Supermarket', 'supermarket', 1, '', '', '', 0),
(427, 3, 74, 'suburb', 'Suburb', '', 1, '', '', '', 0),
(428, 4, 74, 'postcode', 'Postcode', '', 1, '', '', '', 0),
(429, 5, 74, 'latitude', 'Latitude', '', 1, '', '', '', 0),
(430, 6, 74, 'longitude', 'longitude', '', 1, '', '', '', 0),
(431, 1, 75, 'id', 'ID', 'id', 0, '', '', '', 0),
(432, 1, 75, 'name', 'Name', '', 1, '50%', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `modules_fields_validation`
--

DROP TABLE IF EXISTS `modules_fields_validation`;
CREATE TABLE IF NOT EXISTS `modules_fields_validation` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field_id` int(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `modules_fields_validation`
--

INSERT INTO `modules_fields_validation` (`id`, `name`, `field_id`) VALUES
(16, 'instance', 19),
(17, 'instance', 18),
(18, 'instance', 20),
(19, 'unique_current', 18),
(20, 'unique', 8),
(21, 'instance', 8),
(22, 'instance', 14),
(23, 'unique', 35),
(24, 'instance', 35),
(25, 'instance', 9),
(26, 'boolean_true', 19),
(27, 'email', 363),
(28, 'unique', 363),
(29, 'instance', 371),
(30, 'instance', 380);

-- --------------------------------------------------------

--
-- Table structure for table `modules_fields_validation_arguments`
--

DROP TABLE IF EXISTS `modules_fields_validation_arguments`;
CREATE TABLE IF NOT EXISTS `modules_fields_validation_arguments` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `validation_id` int(32) NOT NULL,
  `index` int(2) NOT NULL,
  `value` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `modules_fields_validation_arguments`
--


-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned NOT NULL,
  `ean` decimal(13,0) unsigned NOT NULL,
  `brand` int(10) unsigned NOT NULL,
  `measure` decimal(9,4) unsigned NOT NULL,
  `uom` enum('kg','g','l','ml','per') NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_id` int(3) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_product_types` (`type`),
  KEY `FK_products_brands` (`brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `type`, `ean`, `brand`, `measure`, `uom`, `image`, `category_id`, `name`, `description`) VALUES
(4, 13, '0', 1, '500.0000', 'g', 'arnotts-family_pack.jpg', 4, 'Family Assorted', 'This is a mix of Arnotts biscuits perfect for a family who has a diverse choice of preferred biscuit. '),
(5, 60, '1', 2, '100.0000', 'g', '', 1, NULL, NULL),
(6, 28, '2', 3, '420.0000', 'g', '', 1, NULL, NULL),
(7, 61, '3', 4, '375.0000', 'ml', '', 1, NULL, NULL),
(8, 10, '4', 5, '138.0000', 'g', '', 1, NULL, NULL),
(9, 63, '5', 6, '165.0000', 'g', '', 1, NULL, NULL),
(10, 64, '6', 7, '500.0000', 'g', '', 1, NULL, NULL),
(11, 65, '7', 8, '20.0000', '', '', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_similar`
--

DROP TABLE IF EXISTS `products_similar`;
CREATE TABLE IF NOT EXISTS `products_similar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `similar_to_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_similar_products` (`product_id`),
  KEY `FK_products_similar_products_2` (`similar_to_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `products_similar`
--


-- --------------------------------------------------------

--
-- Table structure for table `product_sales`
--

DROP TABLE IF EXISTS `product_sales`;
CREATE TABLE IF NOT EXISTS `product_sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `supermarket_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `price` decimal(5,2) unsigned DEFAULT NULL,
  `special_price` decimal(5,2) unsigned DEFAULT NULL,
  `bogo_buy_count` int(10) unsigned DEFAULT NULL,
  `bogo_buy_price` decimal(5,2) unsigned DEFAULT NULL,
  `bogo_get_count` int(10) unsigned DEFAULT NULL,
  `bogo_get_price` decimal(5,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_sales_products` (`product_id`),
  KEY `FK_product_sales_supermarkets` (`supermarket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `product_sales`
--

INSERT INTO `product_sales` (`id`, `product_id`, `supermarket_id`, `start_date`, `end_date`, `price`, `special_price`, `bogo_buy_count`, `bogo_buy_price`, `bogo_get_count`, `bogo_get_price`) VALUES
(3, 4, 1, '2012-07-26', '2012-07-29', '6.00', '4.00', NULL, NULL, NULL, NULL),
(4, 5, 2, '2012-08-02', '2012-08-09', '1.75', '1.00', NULL, NULL, NULL, NULL),
(5, 6, 2, '2012-08-02', '2012-08-09', '1.75', '1.00', NULL, NULL, NULL, NULL),
(6, 7, 2, '2012-08-02', '2012-08-09', '0.00', '28.00', NULL, NULL, NULL, NULL),
(7, 8, 2, '2012-08-02', '2012-08-09', '3.99', '1.99', NULL, NULL, NULL, NULL),
(8, 9, 2, '2012-08-02', '2012-08-09', '2.99', '1.70', NULL, NULL, NULL, NULL),
(9, 10, 2, '2012-08-02', '2012-08-09', '7.25', '6.50', NULL, NULL, NULL, NULL),
(10, 11, 2, '2012-08-02', '2012-08-09', '7.99', '7.00', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_sale_exceptions`
--

DROP TABLE IF EXISTS `product_sale_exceptions`;
CREATE TABLE IF NOT EXISTS `product_sale_exceptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_sale_id` int(10) unsigned NOT NULL,
  `exception_store_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_sale_exceptions_products` (`product_sale_id`),
  KEY `FK_product_sale_exceptions_stores` (`exception_store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `product_sale_exceptions`
--


-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

DROP TABLE IF EXISTS `product_types`;
CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `name`, `category_id`) VALUES
(1, 'Baking Agents', 1),
(2, 'bread mix', 1),
(3, 'breadcrumbs & stuffing mixes', 1),
(4, 'cake mix', 1),
(5, 'cooking chocolate', 1),
(6, 'decorations & toppings', 1),
(7, 'dried fruit & nuts', 1),
(8, 'essence & food colouring', 1),
(9, 'flour', 1),
(10, 'juices', 1),
(11, 'spices', 1),
(12, 'sugar', 1),
(13, 'chocolate', 2),
(14, 'crackers & crispbreads', 2),
(15, 'cream & plain', 2),
(16, 'savoury biscuits', 2),
(17, 'specialty', 2),
(18, 'cereal', 3),
(19, 'health & organic', 3),
(20, 'muesli', 3),
(21, 'oats & porridge', 3),
(22, 'on the go', 3),
(23, 'baked beans', 4),
(24, 'canned fish', 4),
(25, 'canned fruit', 4),
(26, 'canned meats', 4),
(27, 'coconut cream milk', 4),
(28, 'spaghetti', 4),
(29, 'chips', 5),
(30, 'corn chips & salsa', 5),
(31, 'crackers & crispbread', 5),
(32, 'flavoured snacks & biscuits', 5),
(33, 'healthy snacks', 5),
(34, 'muesli & snack bars', 5),
(35, 'nuts', 5),
(36, 'other snacks & nibbles', 5),
(37, 'popcorn & pretzels', 5),
(38, 'snack packs', 5),
(39, 'bake at home', 6),
(40, 'bread', 6),
(41, 'bread rolls', 6),
(42, 'cakes & snacks', 6),
(43, 'crumpets & muffins', 6),
(44, 'flat breads', 6),
(45, 'freshly baked bread', 6),
(46, 'fruit loaf', 6),
(47, 'garlic & specialty breads', 6),
(48, 'shells & cases', 6),
(49, 'bacon', 7),
(50, 'cheese', 7),
(51, 'frankfurts & sausages', 7),
(52, 'olives & antipasto', 7),
(53, 'platters', 7),
(54, 'poultry', 7),
(55, 'pre packed meats', 7),
(56, 'ready meals', 7),
(57, 'salads', 7),
(58, 'sliced meats', 7),
(59, 'twiggy sticks & kabana', 7),
(60, 'Heinz', NULL),
(61, 'Coca Cola', NULL),
(62, 'Kellogg''s', NULL),
(63, 'Arnott''s', NULL),
(64, 'Coles', NULL),
(65, 'Stayfree', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shopping_lists`
--

DROP TABLE IF EXISTS `shopping_lists`;
CREATE TABLE IF NOT EXISTS `shopping_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `date` date NOT NULL,
  `total_rrp` float(5,2) NOT NULL,
  `total_special` float(5,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `shopping_lists`
--

INSERT INTO `shopping_lists` (`id`, `name`, `date`, `total_rrp`, `total_special`) VALUES
(1, 'temp', '0000-00-00', 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `shopping_lists_products`
--

DROP TABLE IF EXISTS `shopping_lists_products`;
CREATE TABLE IF NOT EXISTS `shopping_lists_products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shopping_list_product_id` int(10) DEFAULT '0',
  `shopping_list_id` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `shopping_lists_products`
--

INSERT INTO `shopping_lists_products` (`id`, `shopping_list_product_id`, `shopping_list_id`) VALUES
(60, 8, 1),
(63, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supermarket_id` int(10) unsigned NOT NULL,
  `suburb` varchar(255) NOT NULL,
  `postcode` char(4) NOT NULL,
  `latitude` decimal(9,6) DEFAULT NULL,
  `longitude` decimal(9,6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_stores_supermarkets` (`supermarket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `supermarket_id`, `suburb`, `postcode`, `latitude`, `longitude`) VALUES
(1, 2, '', '2031', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supermarkets`
--

DROP TABLE IF EXISTS `supermarkets`;
CREATE TABLE IF NOT EXISTS `supermarkets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `supermarkets`
--

INSERT INTO `supermarkets` (`id`, `name`) VALUES
(2, 'Coles'),
(1, 'Woolworths');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified` tinyint(1) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastlogin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_registered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `temporary_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `yahoo_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `windows_live_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_code` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `email_verified`, `password`, `lastlogin`, `lastip`, `group_id`, `avatar`, `date_registered`, `display_name`, `temporary_password`, `facebook_id`, `twitter_id`, `yahoo_id`, `windows_live_id`, `google_id`, `type`, `post_code`) VALUES
(1, 'tomo@noddingdogdesign.com', 1, '087d7bdb0ddab2efe969c4576f228110b4f31bea', '2012-08-02 23:45:47', '::1', '1', 'tpl/uploads/2012-03-08-15-51-01189.jpg', '2012-07-18 09:23:51', 'admin', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '', '', '', '', '', 'core', 0),
(2, 'tomraine88@gmail.com', 0, 'fe9e0e798d60bbb3afa5060c9ad5888e21ba7ac8', '0000-00-00 00:00:00', '', '2', '', '2012-07-18 11:21:15', 'Tomo', '', '', '', '', '', '', 'core', 0),
(3, 'adrian@noddingdogdesign.com', 1, '8308550b79973e5e455cb4101d0bda6847966c8b', '2012-07-19 14:35:43', '::1', '2', '', '2012-07-19 14:07:41', 'Adiran', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '', '', '', '', '', 'core', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `default_value` tinyint(1) NOT NULL,
  `access_level` bigint(32) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `name`, `admin`, `default_value`, `access_level`) VALUES
(1, 'Administrators', 1, 0, 3),
(2, 'Members', 0, 1, 1),
(3, 'Data Input', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_meta`
--

DROP TABLE IF EXISTS `users_meta`;
CREATE TABLE IF NOT EXISTS `users_meta` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `user` bigint(32) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users_meta`
--

INSERT INTO `users_meta` (`id`, `key`, `value`, `user`) VALUES
(1, 'verification_code', 'UBxk4ZqjDiASIJQ0T2z9', 2),
(2, 'verification_code', 'DJNtsuUHT9qHrOAj156S', 3),
(4, 'post_code', '2293', 1),
(5, 'name', 'Thomas Raine', 1),
(6, 'website', 'http://tomraine.com', 1),
(7, 'date_of_birth', '1988-03-14', 1),
(8, 'gender', 'Male', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `aisle_id` FOREIGN KEY (`aisle_id`) REFERENCES `aisle` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_products_brands` FOREIGN KEY (`brand`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `FK_products_product_types` FOREIGN KEY (`type`) REFERENCES `product_types` (`id`);

--
-- Constraints for table `products_similar`
--
ALTER TABLE `products_similar`
  ADD CONSTRAINT `FK_products_similar_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FK_products_similar_products_2` FOREIGN KEY (`similar_to_product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `product_sales`
--
ALTER TABLE `product_sales`
  ADD CONSTRAINT `FK_product_sales_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FK_product_sales_supermarkets` FOREIGN KEY (`supermarket_id`) REFERENCES `supermarkets` (`id`);

--
-- Constraints for table `product_sale_exceptions`
--
ALTER TABLE `product_sale_exceptions`
  ADD CONSTRAINT `FK_product_sale_exceptions_products` FOREIGN KEY (`product_sale_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `FK_product_sale_exceptions_stores` FOREIGN KEY (`exception_store_id`) REFERENCES `stores` (`id`);

--
-- Constraints for table `product_types`
--
ALTER TABLE `product_types`
  ADD CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `FK_stores_supermarkets` FOREIGN KEY (`supermarket_id`) REFERENCES `supermarkets` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
