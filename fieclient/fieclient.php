<?php
/**
 * At it's core, this file makes calls out to the currently existing Java
 * FIEClient code. However, it also provides a number of helper functions
 * that exist to help with the intended day-to-day operations of the 
 * code that is calling this file.
 * 
 * @author Lincoln Maskey <lincoln@maskey.net>
 * @copyright 2012
 */
function SetWorkingDirectory() {
	// chdir(realpath(dirname(__FILE__) . '/fieclient'));
	chdir(realpath(dirname(__FILE__)));
}

function GetJavaExecLine() {
	$exec_line = 'java ';
	$exec_line .= '-cp .:dsn.jar:imap.jar:ini4j-0.5.2.jar:json-lib-2.4-jdk15.jar:mailapi.jar:pop3.jar:smtp.jar:soap-2.3.1.jar ';
	$exec_line .= 'FIEClient ';
	return $exec_line;
}

function CallJavaApplicationWithParameters($function_to_call, $parameters) { 
	SetWorkingDirectory();

	$exec_line = GetJavaExecLine();
	$exec_line .= ' ' . escapeshellcmd(trim($function_to_call));
	foreach ($parameters as $parameter) {
		// escapeshellcmd breaks the apostrophes that we may give...
		$exec_line .= ' "' . str_replace('"', '\\"', trim($parameter)) . '"';
	}

	$output_array = array();
	$return_val = 0;
	exec($exec_line, $output_array, $return_val);
	
	$output_line = trim($output_array[0]);
	$output_parts = explode('|', $output_line);
	
	$return_array = array();
	$return_array['return_val'] = $return_val;
	$return_array['return_val_output'] = $output_parts[0];
	$return_array['result_text'] = $output_parts[1];
	$return_array['request_id'] = $output_parts[2];
	$return_array['downloads_location'] = $output_parts[3];

	// Exit codes are capped at 256 but our error codes go all the way to 999...
// 	if ($return_array['return_val'] != $return_array['return_val_output']) {
// 		var_dump($return_array);
// 		throw new Exception();
// 	}
	
	return $return_array;
}

function GetMatchingProducts($supplier, $product_name, &$initial_call_return_array = array()) { 
	$return_array = CallJavaApplicationWithParameters('get-matching-products', array($supplier, $product_name));
	$initial_call_return_array = $return_array;
	
	$return_val = $return_array['return_val'];
	$return_val_output = $return_array['return_val_output'];
	$result_text = $return_array['result_text'];
	$request_id = $return_array['request_id'];
	$downloads_location = $return_array['downloads_location'];
	
	if ($return_val != 0) {
		return array();
	}
	
	if ($request_id == '') {
		return array();
	}

	$the_filename = $downloads_location . '/' . $request_id . '.xml';
	$xml = new SimpleXMLIterator($the_filename, 0, true);

	// Pull the XML apart...
	$matching_products = array();
	
	$meta_data_values = $xml->{'meta-data-values'}[0];
	foreach($meta_data_values->image as $image) {
		$matching_product = array();
		$matching_product['image_id'] = (string)$image['id'];
		
		foreach ($image->{'meta-data-value'} as $meta_value) {
			if ($meta_value->{'key-name'} == 'description') {
				$matching_product['description'] = (string)$meta_value->{'key-value'};
			}
			if ($meta_value->{'key-name'} == 'ean') {
				$matching_product['ean'] = (string)$meta_value->{'key-value'};
			}
			if ($meta_value->{'key-name'} == 'supplier') {
				$matching_product['supplier'] = (string)$meta_value->{'key-value'};
			}
		}
		
		$matching_products[$matching_product['ean']] = $matching_product;
	}
	
	return $matching_products;
}

function GetImagesForEAN($ean, &$initial_call_return_array = array()) { 
	$return_array = CallJavaApplicationWithParameters('get-images-for-ean', array($ean));
	$initial_call_return_array = $return_array;
	
	$return_val = $return_array['return_val'];
	$return_val_output = $return_array['return_val_output'];
	$result_text = $return_array['result_text'];
	$request_id = $return_array['request_id'];
	$downloads_location = $return_array['downloads_location'];
	
	if ($return_val != 0) {
		return array();
	}
	
	if ($request_id == '') {
		return array();
	}

	$the_filename = $downloads_location . '/' . $request_id . '.xml';
	$xml = new SimpleXMLIterator($the_filename, 0, true);

	// Pull the XML apart...
	$image_id_2d = '';
	$image_id_3d = '';
	
	$meta_data_values = $xml->{'meta-data-values'}[0];
	foreach ($meta_data_values->image as $image) {
		if (! $image_id_2d) {
			$image_id = (string)$image['id'];
			
			if (substr($image_id, -3) == '-2D') {
				foreach ($image->{'meta-data-value'} as $meta_value) {
					if ($meta_value->{'key-name'} == 'ean') {
						if ($meta_value->{'key-value'} == $ean) {
							$image_id_2d = $image_id;
						}
					}
				}
			} else if (substr($image_id, -3) == '-3D') {
				foreach ($image->{'meta-data-value'} as $meta_value) {
					if ($meta_value->{'key-name'} == 'ean') {
						if ($meta_value->{'key-value'} == $ean) {
							$image_id_3d = $image_id;
						}
					}
				}
			}
		}
	}

	$image_id = $image_id_2d;
	if (! $image_id) {
		$image_id = $image_id_3d;
	}

	if (! $image_id) {
		return false;
	}
	
	$the_filename = $downloads_location . '/' . $request_id . '.zip';

	$zip = new ZipArchive();
	$result = $zip->open($the_filename);
	if ($result === true) {
		$zip->extractTo($downloads_location, array($image_id . '.jpg'));
		$zip->close();
		
		return $downloads_location . '/' . $image_id . '.jpg';
	}
	
	return false;
}

/**
 * For a given EAN, retrieve the image for the EAN and copy it to the 
 * given directory.
 * 
 * @param unknown_type $the_ean
 * @param unknown_type $the_directory
 */
function CopyImageForGivenEANToGivenDirectory($the_ean, $the_directory) {
	$image_path = GetImagesForEAN($the_ean);
	$file_name = basename($image_path);
	
	if ($image_path) {
		copy($image_path, $the_directory . '/' . $file_name);
		return $the_directory . '/' . $file_name;
	} else {
		return '';
	}
}

/**
 * For a given supplier and product name, see if we can find a product
 * whose actual description includes the given measurement.
 * 
 * @param unknown_type $supplier
 * @param unknown_type $product_name
 * @param unknown_type $measurement
 */
function GetMatchingProductWithMeasurement($supplier, $product_name, $measurement, &$initial_call_return_array = array()) {
	$possible_products = GetMatchingProducts($supplier, $product_name, $initial_call_return_array);
	
	// Do any of the descriptions contain the measurement exactly?
	$likely_products = array();
	$search_measurement = $measurement;
	foreach ($possible_products as $ean => $possible_product) {
		if (stripos($possible_product['description'], $search_measurement) !== false) {
			$likely_products[$ean] = $possible_product;
		}
	}
	
	if (count($likely_products)) {
		return $likely_products;
	}
	
	// What about containing just the number, then?
	$search_measurement = preg_replace('/[^0-9]/', '', $measurement);
	foreach ($possible_products as $ean => $possible_product) {
		if (stripos($possible_product['description'], $search_measurement) !== false) {
			$likely_products[$ean] = $possible_product;
		}
	}
	
	if (count($likely_products)) {
		return $likely_products;
	}
	
	return array();
}

/*
// Single image EAN:
echo GetImagesForEAN('9310072025155') . "\n";
// EAN with both a 2D and a 3D image:
echo GetImagesForEAN('9300657035044') . "\n";
var_dump(GetMatchingProducts('arnott', 'tim tam'));
echo CopyImageForGivenEANToGivenDirectory('9310072025155', dirname(__FILE__) . '/images');
var_dump(GetMatchingProductWithMeasurement('arnott', 'tim tam', '175g'));
var_dump(GetMatchingProductWithMeasurement('arnott', 'tim tam', '200'));
var_dump(GetMatchingProductWithMeasurement('arnott', 'tim tam', '9pk'));
*/