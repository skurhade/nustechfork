<?php
class WindowsLive
{
	
	const WRAP_SCOPE = 'WL_Profiles.View';
	const WRAP_CONSENT_URL = 'https://consent.live.com/Connect.aspx';
	const WRAP_ACCESS_URL = 'https://consent.live.com/AccessToken.aspx';
	const WRAP_REFRESH_URL = 'https://consent.live.com/RefreshToken.aspx';

	protected $client_id = '';
	protected $client_secret = '';
	protected $callback = '';

	public function __construct($client_id, $client_secret, $callback)
	{
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
		$this->callback = $callback;
	}

    /**
     * Issues a request to the LIVE services for a consent token by redirecting the
     * user to the LIVE WRAP authentication page and at the same time setting the
     * application ID and callback url.
     *
     * @param string $wrapScope   The scope of access you wish to request for
     *                            this application, the default value is an empty
     *                            string.
     *
     * @return null
     */
    public function getConsentToken($wrapScope = '') {
        $wrap_consent_request = self::WRAP_CONSENT_URL  .'?'
                . 'wrap_client_id=' . urlencode($this->client_id)
                . '&wrap_callback=' . urlencode($this->callback)
                . '&wrap_scope=' . urlencode(self::WRAP_SCOPE);
        return $wrap_consent_request;
    }

    /**
     * Issues an synchronous authorization request by generating a https POST
     * request to the LIVE authentication servers. The result, whether successful
     * or unsuccessful is returned to the calling function, parsed and displayed
     * to the user and saved to the session.
     *
     * @param string $verificationCode The verification code that was returned
     * as part of the consent request, which has an expiry.
     *
     * @return null
     */
    public function getAuthorizationToken($verificationCode) {
        // Using the returned verification code build a query to the
        // authorization url that will return the authorized verification code

        $tokenRequest = 'wrap_client_id=' . urlencode($this->client_id)
                . '&wrap_client_secret=' . urlencode($this->client_secret)
                . '&wrap_callback=' . urlencode($this->callback)
                . '&wrap_verification_code=' . urlencode($verificationCode);
        $response = $this->_postWRAPRequest(self::WRAP_ACCESS_URL, $tokenRequest);
        return $this->parsePOSTResponse($response);
    }
	
	public function getProfile( $cid, $access_token )
	{
		$url_string = 'http://apis.live.net/V4.1/cid-'.$cid.'/Profiles/';
		
		$curl_session = curl_init($url_string);
		
		// build HTTP header with authorization code
		$curl_options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => array(
				'Authorization: WRAP access_token="'.urlencode($access_token).'"',
				'Accept: application/json'
			)
		);
		
		// setup options for curl transfer
		curl_setopt_array($curl_session, $curl_options);
		
		// execute session and get response
		$curl_response = curl_exec($curl_session);
		curl_close($curl_session);

		$entries = json_decode($curl_response, true);
		$entries = $entries['Entries'];
		$entries = array_pop($entries);
		return $entries;
		
	}

    /**
     * Called to refresh an authorization token using the refresh token that
     * is returned after a consent token is first authorized.
     *
     * @param string $refreshToken The token that is returned as part of the
     *                             authorization request.
     *
     * @return null
     */
    public function getRefreshedToken($refreshToken) {
        // Using the returned verification code build a query to the
        // authorization url that will return the authorised verification code

        $tokenRequest = 'wrap_refresh_token=' . urlencode($refreshToken)
                . '&wrap_client_id=' . urlencode($this->client_id)
                . '&wrap_client_secret=' . urlencode($this->client_secret);

        $response = $this->_postWRAPRequest(self::WRAP_REFRESH_URL, $tokenRequest);
        return $this->parsePOSTResponse($response);
    }

    /**
     * Issues a synchronous http POST request to a url and returns the response
     * headers as well as the response content in a single string.
     *
     * @param string $posturl  The web url where the POST will be directed.
     * @param string $postvars The post variables that you are issuing as part
     *                         of the POST request. They must be in the format
     *                         var1=val1&var2=va12.
     *                         Note that there is no leading '?' and also not
     *                         that the individual values
     *                         need to be urlencoded but the $postvars string
     *                         itself must not be.
     *
     * @return null
     */
    private function _postWRAPRequest($posturl, $postvars)
    {
        $ch = curl_init($posturl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        // On Windows machines this prevents cURL from falling over when
        // requesting SSL urls
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $rec_Data = curl_exec($ch);
        curl_close($ch);

        return urldecode($rec_Data);
    }

    /**
     * Extract from the POST response any returned variables whether they be error
     * values or expected values.
     *
     * @param string $response The HTTP response string or query string that
     *                         contains the header and html string to format.
     * 
     * @return null
     */
    public function parsePOSTResponse($response)
    {
        // Firstly remove any extraneous header information from the returned
        // HTML
        if (strpos($response, '?') === false) {
            $pos = strpos($response, 'wrap_access_token=');

            if ($pos === false) {
                $pos = strpos($response, 'wrap_error_reason=');
            }
            if ($pos !== false) {
                $response = '?' . substr($response, $pos, strlen($response));
            }
        }
        $returnedVariables = array();
        // RegEx the string to separate out the variables and their values
        if (preg_match_all('/[?&]([^&=]+)=([^&=]+)/', $response, $matches)) {
            $contents = '';
            for ($i =0; $i < count($matches[1]); $i++) {
                $_SESSION[urldecode($matches[1][$i])]
                    = urldecode($matches[2][$i]);
                $returnedVariables[urldecode($matches[1][$i])]
                    = urldecode($matches[2][$i]);
            }
        } else {
            throw new UnexpectedValueException(
                    'There are no matches for the regular expression used
                        against the OAuth response.');
        }
        return $returnedVariables;
    }

    /**
     * Retreives the value of a parameter whether it has stored in the session or
     * has been passed as a $_REQUEST parameter. If the parameter is not found
     * in either the session or $_REQUEST array then a default value is returned .
     *
     * @param string $parameterName The name of parameter to be searched.
     * @param string $returnDefault The default value you want returned if there
     *                              is no value present in either
     *                              $_REQUEST or $_SESSION.
     *
     * @return The value of the parameter present in either the $_REQUEST,
     * $_SESSION or a default value.
     */
    public function getReturnedParameter($parameterName, $returnDefault = '')
    {
        $value = $returnDefault;
        if (isset($_REQUEST[$parameterName]) && $_REQUEST[$parameterName] != '') {
            $value = $_REQUEST[$parameterName];
            $_SESSION[$parameterName] = $value;
        } else if (isset($_SESSION[$parameterName])) {
            $value = $_SESSION[$parameterName];
        }
        return $value;
    }  
}
?>
