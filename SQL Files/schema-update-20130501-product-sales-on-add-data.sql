ALTER TABLE `product_sales`
	ADD COLUMN `added_datetime` DATETIME NULL DEFAULT NULL AFTER `promo_type`,
	ADD COLUMN `added_by_user_id` INT UNSIGNED NULL DEFAULT NULL AFTER `added_datetime`,
	ADD COLUMN `added_session_id` VARCHAR(255) NULL DEFAULT NULL AFTER `added_by_user_id`,
	ADD COLUMN `added_csv_filename` VARCHAR(255) NULL DEFAULT NULL AFTER `added_session_id`,
	ADD COLUMN `added_csv_line_number` INT UNSIGNED NULL DEFAULT NULL AFTER `added_csv_filename`,
	ADD COLUMN `last_modified_datetime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `added_csv_line_number`;

