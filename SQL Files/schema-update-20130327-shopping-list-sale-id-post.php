<?php
require_once(dirname(__FILE__).'/../functions.php');
require_once(dirname(__FILE__).'/../config.php');

$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
echo "<pre>\n";

echo "Remove bad shopping list items due to no postcode and / or no supermarket\n";
$query = "DELETE FROM shopping_lists_products WHERE postcode=0 OR postcode IS NULL OR supermarket_id=0 OR supermarket_id IS NULL";
$db_link->query($query);
echo "Deleted " . $db_link->affected_rows . " rows\n";

echo "Remove bad shopping list items due to no longer existing products\n";
$query = "DELETE FROM shopping_lists_products WHERE shopping_list_product_id NOT IN (SELECT id FROM products)";
$db_link->query($query);
echo "Deleted " . $db_link->affected_rows . " rows\n";

$query = "SELECT id, shopping_list_product_id, supermarket_id, postcode FROM shopping_lists_products";
$db_result = $db_link->query($query);

echo "Setting shopping_list_product_sale_id for " . $db_result->num_rows . " shopping list products\n";
$good_count = 0;
$bad_count = 0;

while ($db_row = $db_result->fetch_assoc()) {
	$id = intval($db_row['id']);
	$shopping_list_product_id = intval($db_row['shopping_list_product_id']);
	$supermarket_id = intval($db_row['supermarket_id']);
	$postcode = intval($db_row['postcode']);
	
	$query = "SELECT id FROM product_sales 
			WHERE product_id=$shopping_list_product_id 
			AND supermarket_id=$supermarket_id
			AND postcode=$postcode";
	
	$sales_result = $db_link->query($query);
	if (! $sales_result) {
		echo "ERROR: $query: " . $db_link->error . "\n";
	}
	
	if ($sales_result->num_rows == 0) {
		// echo "Shopping List Product: $id: could not find matching product: $query\n";
		echo '0';
		die($query);
	} else if ($sales_result->num_rows > 1) {
		// echo "Shopping List Product: $id: found more than one matching product: $query\n";
		echo '+';
		$bad_count += $sales_result->num_rows;
	} else {
		$sales_row = $sales_result->fetch_assoc();
		$sales_row_id = intval($sales_row['id']);
		
		$query = "UPDATE shopping_lists_products 
				SET shopping_list_product_sale_id=$sales_row_id
				WHERE id=$id";
		$update_result = $db_link->query($query);
		
		if (! $update_result) {
			// echo "ERROR: $query: " . $db_link->error . "\n";
			echo '#';
		} else {
			echo '.';
			$good_count += $db_link->affected_rows;
		}
	}
}

echo "\nResult:\n";
echo $good_count . " rows corrected / affected\n";
echo $bad_count . " rows NOT corrected / affected\n";
