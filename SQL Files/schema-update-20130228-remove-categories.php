<?php
require_once(dirname(__FILE__).'/../functions.php');
require_once(dirname(__FILE__).'/../config.php');

$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);

echo '<pre>';
echo "Pulling product types that exist multiple times in aisles...\n";
$query = "SELECT COUNT(id) AS count_id, name, aisle_id 
		FROM product_types 
		GROUP BY name, aisle_id
		HAVING count_id > 1";

$results = $db_link->query($query);

while ($row = $results->fetch_assoc()) {
	$count_id = intval($row['count_id']);
	$name = $row['name'];
	$escaped_name = $db_link->real_escape_string($name);
	$aisle_id = intval($row['aisle_id']);
	
	echo "$count_id x $name in aisle $aisle_id\n";
	$query = "SELECT id FROM product_types WHERE `name`='$escaped_name' AND aisle_id=$aisle_id ORDER BY id ASC";
	$types_results = $db_link->query($query);
	
	$new_id = null;
	$old_ids = array();
	while ($types_row = $types_results->fetch_assoc()) {
		$this_id = intval($types_row['id']);
		
		if ($new_id === null) {
			$new_id = $this_id;
		} else {
			$old_ids[] = $this_id;
		}
	}
	
	$old_ids_string = implode(', ', $old_ids);
	
	// Change all of the necessary products...
	$query = "UPDATE products SET `type`=$new_id WHERE `type` IN ($old_ids_string)";
	$db_link->query($query);
	
	// and products that are in shopping lists...
	$query = "UPDATE shopping_lists_products SET `type`=$new_id WHERE `type` IN ($old_ids_string)";
	$db_link->query($query);
	
	// then delete the old products...
	$query = "DELETE FROM product_types WHERE id IN ($old_ids_string)";
	$db_link->query($query);
	
	if ($db_link->affected_rows != ($count_id - 1)) {
		echo "!! Odd... '$query' affected " . $db_link->affected_rows . " rows, instead of " . ($count_id - 1) . "\n";
	}
}

echo "We can add back the unique index now...\n";
// Drop the current index first, then add it. We do this separately in case we've already removed it, otherwise
// the entire query would fail.
// Drop the current index... 
$query = "ALTER TABLE `product_types` DROP INDEX `name`;";
$db_link->query($query);

// Add the new one back...
$query = "ALTER TABLE `product_types` ADD UNIQUE INDEX `name` (`name`, `aisle_id`);";
$db_link->query($query);

echo "DONE!\n";