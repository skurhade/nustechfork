# --------------------------------------------------------
# Host:                         localhost
# Server version:               5.5.16
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2012-07-24 09:41:00
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping structure for table grocery_mate_products.aisle
DROP TABLE IF EXISTS `aisle`;
CREATE TABLE IF NOT EXISTS `aisle` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `aisle_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

# Dumping data for table grocery_mate_products.aisle: ~11 rows (approximately)
/*!40000 ALTER TABLE `aisle` DISABLE KEYS */;
INSERT INTO `aisle` (`id`, `aisle_name`) VALUES
	(1, 'pantry'),
	(2, 'liquor'),
	(3, 'pet'),
	(4, 'baby'),
	(5, 'health'),
	(6, 'household'),
	(7, 'freezer'),
	(8, 'fridge'),
	(9, 'meat'),
	(10, 'fresh'),
	(11, 'tobacco');
/*!40000 ALTER TABLE `aisle` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.brands
DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table grocery_mate_products.brands: ~0 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.category
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(150) DEFAULT NULL,
  `aisle_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aisle_id` (`aisle_id`),
  CONSTRAINT `aisle_id` FOREIGN KEY (`aisle_id`) REFERENCES `aisle` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

# Dumping data for table grocery_mate_products.category: ~1 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `category_name`, `aisle_id`) VALUES
	(1, 'baking', 1),
	(2, 'biscuits', 1),
	(3, 'breakfast', 1),
	(4, 'canned food', 1),
	(5, 'chips, snacks & nuts', 1),
	(6, 'bakery', 10),
	(7, 'delicatessen', 10),
	(8, 'entertaining packs', 10),
	(9, 'fresh flowers', 10),
	(10, 'fruit', 10),
	(11, 'beef', 9),
	(12, 'deli meats', 9),
	(13, 'game', 9),
	(14, 'lamb', 9),
	(15, 'mince', 9),
	(16, 'cheese', 8),
	(17, 'cream', 8),
	(18, 'custard', 8),
	(19, 'desserts', 8),
	(20, 'dips & antipasto', 8),
	(21, 'chips &  wedges', 7),
	(22, 'desserts', 7),
	(23, 'frozen fruit', 7),
	(24, 'frozen vegitables', 7),
	(25, 'heat & eat', 7);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned NOT NULL,
  `ean` decimal(13,0) unsigned NOT NULL,
  `brand` int(10) unsigned NOT NULL,
  `measure` decimal(9,4) unsigned NOT NULL,
  `uom` enum('kg','g','l','ml','per') NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_id` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_product_types` (`type`),
  KEY `FK_products_brands` (`brand`),
  CONSTRAINT `FK_products_brands` FOREIGN KEY (`brand`) REFERENCES `brands` (`id`),
  CONSTRAINT `FK_products_product_types` FOREIGN KEY (`type`) REFERENCES `product_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table grocery_mate_products.products: ~0 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.products_similar
DROP TABLE IF EXISTS `products_similar`;
CREATE TABLE IF NOT EXISTS `products_similar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `similar_to_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_products_similar_products` (`product_id`),
  KEY `FK_products_similar_products_2` (`similar_to_product_id`),
  CONSTRAINT `FK_products_similar_products_2` FOREIGN KEY (`similar_to_product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_products_similar_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table grocery_mate_products.products_similar: ~0 rows (approximately)
/*!40000 ALTER TABLE `products_similar` DISABLE KEYS */;
/*!40000 ALTER TABLE `products_similar` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.product_sales
DROP TABLE IF EXISTS `product_sales`;
CREATE TABLE IF NOT EXISTS `product_sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `supermarket_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `price` decimal(5,2) unsigned DEFAULT NULL,
  `special_price` decimal(5,2) unsigned DEFAULT NULL,
  `bogo_buy_count` int(10) unsigned DEFAULT NULL,
  `bogo_buy_price` decimal(5,2) unsigned DEFAULT NULL,
  `bogo_get_count` int(10) unsigned DEFAULT NULL,
  `bogo_get_price` decimal(5,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_sales_products` (`product_id`),
  KEY `FK_product_sales_supermarkets` (`supermarket_id`),
  CONSTRAINT `FK_product_sales_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_product_sales_supermarkets` FOREIGN KEY (`supermarket_id`) REFERENCES `supermarkets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table grocery_mate_products.product_sales: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_sales` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.product_sale_exceptions
DROP TABLE IF EXISTS `product_sale_exceptions`;
CREATE TABLE IF NOT EXISTS `product_sale_exceptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_sale_id` int(10) unsigned NOT NULL,
  `exception_store_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_product_sale_exceptions_products` (`product_sale_id`),
  KEY `FK_product_sale_exceptions_stores` (`exception_store_id`),
  CONSTRAINT `FK_product_sale_exceptions_products` FOREIGN KEY (`product_sale_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_product_sale_exceptions_stores` FOREIGN KEY (`exception_store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table grocery_mate_products.product_sale_exceptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_sale_exceptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_sale_exceptions` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.product_types
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE IF NOT EXISTS `product_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table grocery_mate_products.product_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_types` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.stores
DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supermarket_id` int(10) unsigned NOT NULL,
  `suburb` varchar(255) NOT NULL,
  `postcode` char(4) NOT NULL,
  `latitude` decimal(9,6) DEFAULT NULL,
  `longitude` decimal(9,6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_stores_supermarkets` (`supermarket_id`),
  CONSTRAINT `FK_stores_supermarkets` FOREIGN KEY (`supermarket_id`) REFERENCES `supermarkets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

# Dumping data for table grocery_mate_products.stores: ~0 rows (approximately)
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;


# Dumping structure for table grocery_mate_products.supermarkets
DROP TABLE IF EXISTS `supermarkets`;
CREATE TABLE IF NOT EXISTS `supermarkets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Dumping data for table grocery_mate_products.supermarkets: ~0 rows (approximately)
/*!40000 ALTER TABLE `supermarkets` DISABLE KEYS */;
/*!40000 ALTER TABLE `supermarkets` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
