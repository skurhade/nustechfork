CREATE TABLE IF NOT EXISTS `shoppinglists` (
  `listID` int(10) NOT NULL,
  `listTitle` varchar(255) DEFAULT NULL,
  `listDateCreated` date NOT NULL,
  `listDateModified` date DEFAULT NULL,
  `listChildId` int(10) DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `listVisible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`listID`),
  KEY `listChildId` (`listChildId`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `shoppinglists_products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shoppinglists_listID` int(10) NOT NULL,
  `products_type` int(10) unsigned NOT NULL,
  `products_ean` decimal(13,0) unsigned NOT NULL,
  `products_brand` int(10) unsigned NOT NULL,
  `products_measure` decimal(9,4) unsigned NOT NULL,
  `products_uom` enum('kg','g','l','ml','per') NOT NULL,
  `products_image` varchar(255) NOT NULL,
  `products_category_id` int(3) DEFAULT NULL,
  `products_name` varchar(150) DEFAULT NULL,
  `products_description` varchar(300) DEFAULT NULL,
  `product_sales_id` int(10) unsigned NOT NULL,
  `product_sales_product_id` int(10) unsigned NOT NULL,
  `product_sales_supermarket_id` int(10) unsigned NOT NULL,
  `product_sales_start_date` date NOT NULL,
  `product_sales_end_date` date NOT NULL,
  `product_sales_price` decimal(5,2) unsigned DEFAULT NULL,
  `product_sales_special_price` decimal(5,2) unsigned DEFAULT NULL,
  `product_sales_bogo_buy_count` int(10) unsigned DEFAULT NULL,
  `product_sales_bogo_buy_price` decimal(5,2) unsigned DEFAULT NULL,
  `product_sales_bogo_get_count` int(10) unsigned DEFAULT NULL,
  `product_sales_bogo_get_price` decimal(5,2) unsigned DEFAULT NULL,
  `product_types_id` int(10) unsigned NOT NULL,
  `product_types_name` varchar(255) NOT NULL,
  `product_types_category_id` int(3) DEFAULT NULL,
  `category_id` int(3) NOT NULL,
  `category_category_name` varchar(150) DEFAULT NULL,
  `category_aisle_id` int(3) DEFAULT NULL,
  `aisle_id` int(3) NOT NULL,
  `aisle_aisle_name` varchar(150) DEFAULT NULL,
  `brands_id` int(10) unsigned NOT NULL,
  `brands_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
