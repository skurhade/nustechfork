# Bump fields in the Product edit page, to make room for name etc.
UPDATE modules_fields 
	INNER JOIN modules ON modules.id=modules_fields.module_id
	SET modules_fields.order = (modules_fields.order + 3)
	WHERE modules.name='Products'
	AND modules_fields.order > 1;
	
# Add the name field...
INSERT INTO modules_fields (id, `order`, module_id, name, label, `type`, editable)
		VALUES (null, 2, (SELECT id FROM modules WHERE name='Products'), 'name', 'Name', '', 1);

# Add the technical name field...
INSERT INTO modules_fields (id, `order`, module_id, name, label, `type`, editable)
		VALUES (null, 3, (SELECT id FROM modules WHERE name='Products'), 'technical_name', 'Technical Name', '', 1);

# Add the description field...
INSERT INTO modules_fields (id, `order`, module_id, name, label, `type`, editable)
		VALUES (null, 4, (SELECT id FROM modules WHERE name='Products'), 'description', 'Description', 'textarea', 1);
		
		
		
# Fix the product type dropdown...
UPDATE modules_fields 
	INNER JOIN modules ON modules.id=modules_fields.module_id
	SET modules_fields.type = 'product_type', modules_fields.name='type', modules_fields.label='Product Type'
	WHERE modules.name='Products'
	AND modules_fields.name='type';

# Make the name show on the product list, instead of the ID and the product type...
UPDATE modules_fields 
	INNER JOIN modules ON modules.id=modules_fields.module_id
	SET modules_fields.display_width = ''
	WHERE modules.name='Products'
	AND modules_fields.name='id';	
	
UPDATE modules_fields 
	INNER JOIN modules ON modules.id=modules_fields.module_id
	SET modules_fields.display_width = ''
	WHERE modules.name='Products'
	AND modules_fields.type='product_type';	
	
UPDATE modules_fields 
	INNER JOIN modules ON modules.id=modules_fields.module_id
	SET modules_fields.display_width = '20%'
	WHERE modules.name='Products'
	AND modules_fields.name='name';	
	
# Make the product name show instead of the ID when looking at product sales...
UPDATE modules SET field_slug=397 WHERE name='Products' AND `table`='products' AND slug='products';

# Make the sales list order by end date, descending by default
UPDATE modules SET field_orderby=415, orderby_direction='DESC' WHERE name='Product Sales';
