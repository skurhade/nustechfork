<?php
require_once '_inc.php';
require_once 'functions.php';
$head_title = array();
$head_title[] = 'Edit Profile';
$output = '<div id="user-home">';
$output .= '<div id="user-panel">';	

if($user->isAuthorized())
{
	// We get an instance of the users module and the email field to make sure the given email is unique
	$user_module = MK_RecordModuleManager::getFromType('user');
	$field_module = MK_RecordModuleManager::getFromType('module_field');
	$criteria = array(
		array('field' => 'module_id', 'value' => $user_module->getId()),
		array('field' => 'name', 'value' => 'email')
	);
	
	$user_email_field = array_pop( $field_module->searchRecords($criteria) );

	$settings = array(
		'attributes' => array(
			'class' => 'clear-fix titled standard'
		)
	);

	$structure = array(
		'name' => array(
			'label' => 'Real name',
			'value' => $user->getName(),
			'attributes' => array(
				'size' => '40'
			)
		),
		'display_name' => array(
			'label' => 'Display name',
			'validation' => array(
				'instance' => array()
			),
			'value' => $user->getDisplayName(),
			'attributes' => array(
				'size' => '40'
			)
		),
		'email' => array(
			'label' => 'Email',
			'validation' => array(
				'email' => array(),
				'instance' => array(),
				'unique' => array($user, $user_email_field, $user_module)
			),
			'value' => $user->getEmail(),
			'attributes' => array(
				'size' => '40'
			)
		),
		'website' => array(
			'label' => 'Website',
			'validation' => array(
				'url' => array()
			),
			'value' => $user->getWebsite(),
			'attributes' => array(
				'size' => '40'
			)
		),
		'gender' => array(
			'label' => 'Gender',
			'type' => 'select',
			'options' => array(
				'' => 'Prefer not to disclose',
				'Male' => 'Male',
				'Female' => 'Female'
			),
			'value' => $user->getGender()
			
		),
		'date_of_birth' => array(
			'label' => 'Date of Birth',
			'type' => 'date',
			'value' => $user->getDateOfBirth()
		),
		'post_code' => array(
			'label' => 'Post Code',
			'value' => $user->getPostCode(),
			'attributes' => array(
				'size' => '20'
			)
		),
		'avatar' => array(
			'label' => 'Profile image',
			'type' => 'file_image',
			'upload_path' => $config->site->upload_path,
			'value' => $user->getAvatar()
		),
		'submit' => array(
			'type' => 'submit',
			'attributes' => array(
				'value' => 'Save changes'
			)
		)
	);

	$form = new MK_Form($structure, $settings);
	
	if($form->isSuccessful()){
		$output .= '<p class="alert success">Your changes have been saved.</p>';
		$user
			->setAvatar( $form->getField('avatar')->getValue() )
			->setEmail( $form->getField('email')->getValue() )
			->setName( $form->getField('name')->getValue() )
			->setWebsite( $form->getField('website')->getValue() )
			->setDateOfBirth( $form->getField('date_of_birth')->getValue() )
			->setGender( $form->getField('gender')->getValue() )
			->setDisplayName( $form->getField('display_name')->getValue() )
			->setPostCode( $form->getField('post_code')->getValue() )
			->save();
		
		$gm_cookie = new GM_Cookie();
		$gm_cookie->registered_postcode = $form->getField('post_code')->getValue();
		$gm_cookie->WriteBackCookie();
	}
	$output .= '<div id="user-details" class="rounded-corners">';
	$output .= '<h4>Welcome '.ucwords($user->getDisplayName()).'</h4>';	
	$output .= $form->render();
	$output .= '</div>';//end of user-details
	$output .= '</div>';//end of user-panel
	$output .= '</div>';//end of user-home
}else{
	$output .= '<p class="alert warning">Please <a href="login.php">log in</a> or <a href="register.php">register</a> to view this page!</p>';
}

require_once '_header.php';
print $output;
require_once '_footer.php';

?>