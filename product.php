<?php
	require_once('_inc.php') ;
	require_once('functions.php');
	require_once('config.php');
	
	$prod_id = (isset($_GET['pid']) && $_GET['pid'] != '')? $_GET['pid']:die("Product ID not set");
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$now_date_string = date('Y-m-d');
	
	$query = "SELECT products.id, product_sales.id, products.measure, products.uom, products.image, products.description, products.name, products.technical_name,  
					 product_sales.price, product_sales.special_price, product_sales.end_date, product_sales.postcode,
					 brands.name,
					 supermarkets.name
			  FROM products, product_sales, brands, supermarkets
			  WHERE products.id = $prod_id AND 
			  		product_sales.product_id = $prod_id AND 
					brands.id = products.brand AND
					supermarkets.id = product_sales.supermarket_id";
	
	if (true /*$on_sale_only*/) {
		$query .= " AND product_sales.start_date<='$now_date_string' AND product_sales.end_date>='$now_date_string'";
	}
	
	if (true /*$filter_on_user_postcode*/) {
		$gm_cookie = new GM_Cookie();
		if($user->uid)
		{
			$postcode = $user->get_meta('post_code');
				
				
		} else {
			$postcode = $gm_cookie->registered_postcode;
		}
	
			
		if ($postcode && is_numeric($postcode)) {
			$state_code = substr($postcode, 0, 1);
			$state_code_minimum = ($state_code * 1000);
			$state_code_maximum = (($state_code * 1000) + 999);
			$query .= " AND product_sales.postcode >= $state_code_minimum AND product_sales.postcode <= $state_code_maximum";
		}
		// error_log($query);
	}
	
	
	$result = $db_link->prepare($query);
	$result->bind_result($id, $sales_id, $measure, $uom, $image, $description, $name, $technical_name, $price, $special, $sale_end_date, $prod_postcode, $brand, $supermarket);
	$result->execute();
	$result->store_result();
	
	if($result->num_rows < 1){
		die('No products were found');
	}
	$head_title = array();
	
		$head_title[] = 'Product';
	
?>
<?php require_once('_header.php'); ?>

<div id="product-detail">
	<a href="<?php echo $_SESSION['url']; ?>"id="go-back">Back to Product List</a>
	<div id="product-wrapper" class="rounded-corners">
        <div class="product rounded-corners">
        <?php while($result->fetch()): 
				if($user->getId()){
					$postcode = $user->getMetaValue('post_code');	
					
				} else {
					$postcode = $gm_cookie->registered_postcode;
				}
				$state_code = substr($prod_postcode, 0,1);
				$state_code_min = $state_code * 1000;
				$state_code_max = $state_code_min + 999;
				if($postcode >= $state_code_min && $postcode <= $state_code_max){
				
				
		?>
            <div class="product-image rounded-cornersv">
                <!-- Image for product -->
                <img src="images/<?php echo ($image != '')? $image: "default_product.jpg"; ?>" width="227" />
            </div><!-- .product-image -->
            
            <div class="product-detail">
                <!-- Main detials about the product -->
               
                <h2 class="product-name">
                	<?php echo ucwords($brand . ' ' . ($name ? $name : $technical_name)); ?> 
                </h2>
                <h2 class="weight">
                	<?php echo round($measure, 2).$uom; ?>
                </h2>
                <p class="rrp">RRP: $<?php echo $price; ?></p>
                <p class="special">Special: $<?php echo $special; ?></p>
                <?php 
				/*$formatted_sale_date = GetFormattedDBDate($sale_end_date);
				if ($formatted_sale_date) {		
					echo '<p class="sale-end-date">On sale until ' . $formatted_sale_date . '</p>';
				}*/
                ?>
                <div class="product-description">
                    <!-- General description of product -->
                    <img src="images/supermarket/<?php echo strtolower(str_replace(' ', '_', $supermarket)) ?>.png" /><p><?php echo $description; ?></p>
                    <div class="button-add">
                        <!-- button to add item to shopping list -->
                        <form class="add-to-basket" name="add-to-basket" onsubmit="javascript:addToBasket(<?php echo $sales_id ?>); return false;">
           					<input type="submit" class="rounded-corners" id="add" name="add" value="ADD" />
           	   			 	<input type="hidden" name="product_id" id="product_id" value="<?php echo $sales_id ?>" />
            			</form>
                    </div><!-- .button-add -->
                    
                </div><!-- .product-description -->
                
            </div><!-- .product-detail -->
            <div class="button-back">
                <!-- button to go back to search results / browse -->
                
            </div><!-- .button-back -->
            <?php } ?>
            <?php endwhile; ?>
        </div><!-- .product -->
    </div><!-- #product-wrapper -->
    
</div><!-- product-detail -->
<div id="mini-shopping-list">
<?php get_mini_shopping_list() ?>
</div>
<?php require_once('_footer.php'); ?>