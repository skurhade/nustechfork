<?php
require_once '_inc.php';
$head_title = array();
$head_title[] = 'Change Password';

if($user->isAuthorized())
{
	$settings = array(
		'attributes' => array(
			'class' => 'narrow clear-fix standard'
		)
	);

	$structure = array(
		'new_password' => array(
			'label' => 'New password',
			'type' => 'password',
			'validation' => array(
				'instance' => array()
			),
			'attributes' => array(
				'size' => '40'
			)
		),
		'submit' => array(
			'type' => 'submit',
			'attributes' => array(
				'value' => 'Save changes',
				'size' => '40'
			)
		)
	);

	$form = new MK_Form($structure, $settings);
	$output .= '<div id="login" class="rounded-corners">';
	$output .= '<div id="login-form">';
	$output .= '<legend class="form-title top-rounded-corners">Change Password</legend>';

	if($form->isSuccessful())
	{
		$output .= '<p class="alert success">Your changes have been saved.</p>';
		$user
			->setPassword( $form->getField('new_password')->getValue() )
			->save();
	}
	$output .= $form->render();
	$output .= '</div>';
	$output .= '</div>';	
}
else
{
	$output = '<p class="alert success">Please <a href="login.php">log in</a> or <a href="register.php">register</a> to view this page!</p>';
}

require_once '_header.php';
print $output;
require_once '_footer.php';

?>