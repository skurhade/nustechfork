<?php 
	require_once(dirname(__FILE__).'/../functions.php');
	require_once(dirname(__FILE__).'/../config.php');
	//require_once(dirname(__FILE__).'/../_inc.php');
	
	StartSession();
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);

	$query = "SELECT * FROM shopping_lists WHERE user_id = ".$_SESSION['user_id'].";";
	
	$result = $db_link->prepare($query) or die("unable to do query".$db_link->error."user:".$_SESSION['user_id']);
	
	$result->bind_result($id, $list_name, $creation_time, $date, $total_rrp, $total_special, $db_user_id, $visible);
	$result->execute();
	$result->store_result();
	
	$savings = 0;
?>

<div id="shoppinglist-list">
<form action="form-actions/shoppinglist-list-form.php" method="post" name="shoppinglist-list" id="shoppinglist-list-form" class="rounded-corners">
	<fieldset>
    	<legend class="form-title top-rounded-corners cart">SHOPPING LISTS</legend>
        <table width="98%" cellpadding="0" cellspacing="0" align="center">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Savings</th>
                    <th>List</th>
                </tr>
            </thead>
            
            <tbody>
        <?php
			if ($result->num_rows):
				while($result->fetch()):
					if($visible > 0):
		?>
					<tr>
						<td align="center"><?php echo date("d/m/Y", strtotime($date)); ?></td>
						<td align="center"><?php echo $list_name; ?></td>
						<td align="center">$<?php echo number_format(floatval($total_rrp)-floatval($total_special),2); ?></td>
						<td align="center"><!--<input type="checkbox" name="shoppinglist[]" id="shoppinglist-<?php //echo $id ?>" />-->
                        	<a href="main_list_page.php?list_archive=<?php echo $id; ?>">view</a>
                        </td>
					</tr>
		<?php	
					$savings += round(floatval($total_rrp)-floatval($total_special),2);
					endif;
				endwhile;
			else:
		?>
        			<tr>
						<td align="center" colspan="4">You do not have any saved shopping lists</td>
					</tr>
        <?php
			endif;
        ?>
            </tbody>
            <tfoot>
                <tr>
                    <td align="center">Total Saving</td>
                    <td align="center">&nbsp;</td>
                    <td align="center">$<?php echo number_format($savings, 2); ?></td>
                    <td align="center"><input type="checkbox" name="shoppinglist-all" id="shoppinglist-all" /></td>
                </tr>
            </tfoot>
        </table>
    </fieldset>
</form>
</div>
