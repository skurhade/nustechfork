<?php 
	$aisle_id = $_POST['aisle_id'];
	$level = $_POST['level'];
	require_once '../_inc.php';
	require_once(dirname(__FILE__) . '/../functions.php');
	require_once '../config.php'; //config to connect for poduct info not users info
	$demographics = GetDemographicsOfProductsForSale();
	
	$db_link = new MySQLi (DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	$query = "SELECT product_types.id AS id, product_types.name AS name, 
				aisle.id AS aisle_id, aisle_name
				FROM product_types 
				INNER JOIN aisle ON aisle.id=product_types.aisle_id
				WHERE $aisle_id = aisle_id 
				ORDER BY name ASC";

	$results = $db_link->prepare($query);
	
	if ($results) {
		$results->bind_result($id, $name, $aisle_id, $aisle_name);
		$results->execute();
		$results->store_result();
		$row_cnt = $results->num_rows;
	} else {
		die();
	}
?>
<?php						
			
			if (empty($row_cnt) || (! isset($demographics['aisles'][$aisle_id])) || (! $demographics['aisles'][$aisle_id])):
				$output['html'] = '<p>Sorry there are no products in this aisle on special here</p>';
				$output['header'] = "Woops!";
				$output['result'] = false;
			else:
				$output['result'] = true;
				$output['html'] = '<div id="productType" class="browse-list rounded-corners">';
				$output['html'] .= 	'<ul class="category  rounded-corners">';
				while ($results->fetch())
				{
						$name = RewriteSmartQuotes($name);
						$aisle_name = RewriteSmartQuotes($aisle_name);
						
						$hash_url = ('#' . $aisle_id . '-' . $id);
						$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
						$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $name);
						
						// If this product type has no sales, then not only do we have to set the
						// no sale class, but we also have to add some JS to stop the URL actually
						// being changed...
						$this_li = '<li>';
						$this_li .= '<a href="' . $hash_url . '" ';
						
						if (isset($demographics['product_types'][intval($id)])) {
							$this_li .= 'class="' . $id . ' has-sales"';
						} else {
							$this_li .= 'class="' . $id . ' has-no-sales"';
							$this_li .= ' onclick="return false;"';
						}
						
						$this_li .= '>' . ucwords($name) . '</a>';
						$this_li .= '</li>';
						$output['html'] .= $this_li;
					
				}
				$output['html'] .= '</div>';
			endif;			
			
			echo json_encode($output);
?>