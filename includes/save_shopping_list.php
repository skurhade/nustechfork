<?php 

	require_once(dirname(__FILE__).'/../functions.php');
	require_once(dirname(__FILE__).'/../config.php');
	StartSession();
	$output = array();
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	//check if the name has been entered by the user
	if(isset($_POST['name']) && $_POST['name'] != ''):
		$name = $_POST['name'];
	else:
		$output['result'] = false;
		$output['html'] = "Please ensure you have entered a name for this shopping list";
		echo json_encode($output);
		die;
	endif;
	
	//check if the user is registered/logged in
	if(isset($_POST['user_id']) && $_POST['user_id'] != ''):
		$user_id = $_POST['user_id'];
	else:
		$output['result'] = false;
		$output['html'] = "Sorry you must be <a href=\"register.php\" class=\"green\">registered</a>/<a href=\"login.php\" class=\"green\">logged</a> in to save your shopping list";
		echo json_encode($output);
		die;
	endif;

	// Are there any products in this list yet?
	if(isset($_SESSION['list_id']) && $_SESSION['list_id']) {
		$query = "SELECT shopping_list_product_sale_id FROM shopping_lists_products WHERE shopping_list_id=" . intval($_SESSION['list_id']);
		$result = $db_link->query($query);

		if (! $result->num_rows) {
			$output['result'] = false;
			$output['html'] = "Please add at least one product to this list before saving it";
			echo json_encode($output);
			die;
		}
	} else {
		$output['result'] = false;
		$output['html'] = "User: ".$current_list." There was an issue with the shopping list, please try again later";
		echo json_encode($output);
		die;
	}
	
	/*
	 * What are we doing here? 
	 * 
	 * If the last list that we saved has the same name as what we're calling this one,
	 * delete that old one. Then, rename the current temp list with the given name, and
	 * store that name in the session. Finally, start a new temp list.
	 */
	$current_list_id = intval($_SESSION['list_id']);
	
	// Are we just 'updating' the last list that we just saved?
	$last_saved_name = null;
	$last_saved_id = null;
	
	if (isset($_SESSION['list_last_saved_name']) && $_SESSION['list_last_saved_name']) {
		$last_saved_name = $_SESSION['list_last_saved_name'];
	}
	
	if (isset($_SESSION['last_saved_id']) && $_SESSION['last_saved_id']) {
		$last_saved_id = $_SESSION['last_saved_id'];
	}
	
	// Maybe drop the old one?
	if ($last_saved_id && $last_saved_name && ($last_saved_name == $name)) {
		$query = "DELETE FROM shopping_lists_products WHERE shopping_list_id=$last_saved_id";
		$result = $db_link->query($query);
		
		$query = "DELETE FROM shopping_lists WHERE id=$last_saved_id";
		$result = $db_link->query($query);
	}
	
	//save the list with the name and date, if it doesn't work let the user know.
	$escaped_name = $db_link->real_escape_string($name);
	$query = "UPDATE shopping_lists SET name = '$escaped_name', date = CURDATE(), user_id = IFNULL(user_id, $user_id) WHERE id = $current_list_id";
	
	if($result = $db_link->query($query)):
		$output['result'] = true;
		$output['html'] = "Your shopping list has been saved as <b>$name</b>";
		
		$_SESSION['list_last_saved_name'] = $name;
		$_SESSION['last_saved_id'] = $current_list_id;
		
		$new_list_id = get_last_id();
		$_SESSION['list_id'] = $new_list_id;
		duplicate_old_list($new_list_id, $current_list_id);
	else:
		$output['result'] = false;
		$output['html'] = "There was a problem saving your shopping list, please try again later: ".$db_link->error;
	endif;
	
	echo json_encode($output);	

?>
<?php
	function duplicate_old_list ($new_list, $old_list_id){
		$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$query = " INSERT INTO shopping_lists_products (shopping_list_product_count, shopping_list_product_id, shopping_list_product_sale_id, shopping_list_id, supermarket_id, postcode, price, special_price, type, brand, measure, uom, image, name, description) 
				   SELECT shopping_list_product_count, shopping_list_product_id, shopping_list_product_sale_id, $new_list, supermarket_id, postcode, price, special_price, type, brand, measure, uom, image, name, description
				   FROM shopping_lists_products
				   WHERE shopping_list_id = $old_list_id;";
		$sums = "UPDATE shopping_lists SET total_rrp = (SELECT SUM(price * shopping_list_product_count) AS total_rrp FROM shopping_lists_products WHERE shopping_list_id = $old_list_id), 
										   total_special = (SELECT SUM(special_price * shopping_list_product_count) AS total_special FROM shopping_lists_products WHERE shopping_list_id = $old_list_id) 
									   WHERE id = $new_list;";
		try{
			$db_link->query($query);
			$db_link->query($sums);
		}catch(Exception $e){
			echo "There was a problem duplicating old list: ".$e;
		}
	}
?>