<?php
	/*
	/* This form has the form-actions/login-form.pgp action 
	*/
?>
<?php if (isset($_GET['error']) && $_GET['error'] == 'true'): ?>
	<div id="error-message">
    	<?php echo $_SESSION['error'] ?>
    </div>
<?php endif; ?>
<div id="login">
	<form action="form-actions/login-form.php" name="login" id="login-form" method="post">
    	<fieldset>
        	<legend class="form-title">LOGIN</legend>
            <input type="text" name="username" id="username" placeholder="Username" size="40" />
            <input type="password" name="password" id="password" placeholder="Password" size="40" />
            <br />
            <input type="submit" name="login-submit" id="login-submit" value="Submit" class="rounded-corners" />
        </fieldset>
    </form>
	<p>Not registered? <a href="?page=register-form">Click Here</a></p>
</div>