<?php
	require_once(dirname(__FILE__).'/../functions.php');
	require_once(dirname(__FILE__).'/../config.php');
	StartSession();
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) 
		or die("There was an error connecting to the database: ".$db_link->error);
	$product_type_id = $_POST['productType'];
	$on_sale_only = (isset($_POST['onSaleOnly']) && $_POST['onSaleOnly']);
	$now_date_string = date('Y-m-d');
	
	$query = "SELECT products.id, product_sales.id, products.measure, products.uom, products.image,
					 product_sales.price, product_sales.special_price, product_sales.end_date,
					 brands.name, products.name, products.technical_name,
					 product_types.name,
					 supermarkets.name,
					 aisle.id AS aisle_id, aisle_name 
			  FROM products, product_sales, brands, product_types, supermarkets, aisle 
			  WHERE products.type = $product_type_id AND 
			  		products.brand = brands.id AND 
					product_sales.product_id = products.id AND
					products.type = product_types.id AND
					product_types.aisle_id = aisle.id AND
					product_sales.supermarket_id = supermarkets.id";

	if ($on_sale_only) {
		$query .= " AND product_sales.start_date<='$now_date_string' AND product_sales.end_date>='$now_date_string'";
	}
	
	if (true /*$filter_on_user_postcode*/) {
		$gm_cookie = new GM_Cookie();
		if($user->uid)
		{
			$postcode = $user->get_meta('post_code');
			
			
		} else {
			$postcode = $gm_cookie->registered_postcode;
		}
		
			
		if ($postcode && is_numeric($postcode)) {
			$state_code = substr($postcode, 0, 1);
			$state_code_minimum = ($state_code * 1000);
			$state_code_maximum = (($state_code * 1000) + 999);
			$query .= " AND product_sales.postcode >= $state_code_minimum AND product_sales.postcode <= $state_code_maximum";
		} else {
			$query .= " GROUP BY products.id, supermarkets.name";
		}
		// error_log($query);
	}
	
	$results = $db_link->prepare($query);

		$results->bind_result($id, $sale_id, $measure, $uom, $image, $price, $special, $sale_end_date, $brand, $name, $technical_name, $product_type, $supermarket, $aisle_id, $aisle_name);
		$results->execute();
		$results->store_result();
		$row_cnt = $results->num_rows;
		
		$odd_even = "odd";
	$output = array();
	//echo $row_cnt;
	if(empty($row_cnt)):
		$output['result'] = false;
		$output['html'] = '<p>Sorry we cant find any products on special in this Aisle.</p>';
		$output['header'] = '<p></p>';
	else :
		$output['result'] = true;

		$output['html'] = '';
		$first_row = true;
		while($results->fetch())
		{	
			$product_image = ($image != '')?$image:"default_product.jpg";
			if ($first_row) {
				$output['html'] .= '<div class="hidden" id="product-type">'.ucwords($aisle_name).' &gt; '.ucwords($product_type).'</div>';
				$first_row = false;
			}
			//get image size
			
			$image_size = array();
			$image_size = getimagesize(dirname(__FILE__).'/../images/'.$product_image);
			$image_width = $image_size[0];
			$image_height = $image_size[1];
			
			$brand = RewriteSmartQuotes($brand);
			$name = RewriteSmartQuotes($name);
			$technical_name = RewriteSmartQuotes($technical_name);
			$product_type = RewriteSmartQuotes($product_type);
			$aisle_name = RewriteSmartQuotes($aisle_name);
				
			$output['html'] .= '<div class="product rounded-corners '.$odd_even.'">';
			$output['html'] .= '<table cellpadding="0" cellspacing="0" width="100%">';
			$output['html'] .= '	<tr>';
			$output['html'] .= '		<td class="image"><img class="product-thumb rounded-corners" src="images/'.$product_image.'" style="vertical-align:top"';
			$output['html'] .= ($image_size[0] > $image_size[1])? 'width="130"' : 'height="160"';
			$output['html'] .= ' /></td>';
			$output['html'] .= '		<td class="product-data">';
			$output['html'] .= '			<table cellpadding="0" cellspacing="0" width="100%">';
			$output['html'] .= '				<tr><td class="product-name">'.ucwords($brand . ' ' . ($name ? $name : $technical_name)).'</td>';
			$output['html'] .= '				<tr><td class="weight">'.round($measure,2) . ' ' . $uom .'</td></tr>';
			$output['html'] .= '				<tr><td class="price">RRP: $' . $price . '</td></tr>';
			$output['html'] .= '				<tr><td class="special">Special: $'.$special.'</td></tr>';	

			/*$formatted_sale_date = GetFormattedDBDate($sale_end_date);
			if ($formatted_sale_date) {		
				$output['html'] .= '				<tr><td class="sale-date">On sale until ' . $formatted_sale_date . '</td></tr>';			
			}*/
			$output['html'] .= 				'</table>';
			$output['html'] .= '		</td>';
			$output['html'] .= '	</tr>';
			$output['html'] .= '	<tr>';
			$output['html'] .= '		<td class="supermarket"><img src="images/supermarket/'.strtolower(str_replace(' ', '_',$supermarket)).'.png" width="80"/></td>';
			$output['html'] .= ' 		<td class="forms"><div class="product-form">';
			$output['html'] .= '				<form class="view-product" name="view-product" id="view-'.$id.'" method="get" action="product.php">';
			$output['html'] .= '					<input type="submit" class="rounded-corners" name="view" id="view-'.$id.'" value="VIEW" />';
			$output['html'] .= '					<input type="hidden" name="pid" value="'.$id.'" />';
			$output['html'] .= '				</form>';
			$output['html'] .= '				<form class="add-to-basket" name="add-to-basket" onsubmit="javascript:addToBasket('.$sale_id.'); return false;">';
            $output['html'] .= '					<input type="submit" class="rounded-corners" id="add" name="add" value="ADD" />';
            $output['html'] .= '	   			 	<input type="hidden" name="product_id" id="product_id" value="'.$sale_id.'" />';
            $output['html'] .= '				</form>';
			$output['html'] .= '			</div>';
			$output['html'] .= '		</td>';
			$output['html'] .= '	</tr>';	
			$output['html'] .= '</table>';
			$output['html'] .= '</div>';
			$_SESSION['url'] = $_POST['url'];
			$hash_url = ('#' . $aisle_id);
			$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
			$output['parent_hash_url'] = $hash_url;
			
			$odd_even = ($odd_even == "odd")? "even" : "odd";          
		}
	endif;
	echo json_encode($output);
	
	
?>